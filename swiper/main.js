// 图集组件
class XjSwiper {
	constructor(data) {
		this.num = data.num || 0 // 初始化 显示第几张图片
		this.label = data.label || 'url' // 图片地址对应的key
		this.name = data.name // 初始化 放在这个盒子里
		this.dom = document.querySelector(data.name);
		this.width = data.width || 200 // 大图宽度
		this.height = data.height || 200 // 大图高度
		this.minWidth = data.minWidth || 60 // 小图高度
		this.minHeight = data.minHeight || 60 // 小图高度
		this.iconSize = data.iconSize || '20px' // 图标大小
		this.borderRadius = data.borderRadius || '6px' // 大图圆角 
		this.minBorderRadius = data.minBorderRadius || '4px' // // 小图圆角 
		this.activeBorder = data.activeBorder || '1px solid red' // 选中边框
		this.swiperList = data.swiperList || [] // 要渲染的图片集合
		this.indexShow = data.indexShow || false // 是否显示索引
		
		// 处理 left 用的
		this.newNum = 1 
		this.newNumFlag = true
		
		// 大图点击事件
		this.maxClick = (row) => {
			// 利用深拷贝  实现点击事件的响应
			data.maxClick(row)
		}
	}

	initSwiper() {
		let strHtml = ''
		this.swiperList.forEach((item, index) => {
			strHtml +=
				`<div style="width: ${this.minWidth}px;height: ${this.minHeight}px;border-radius: ${this.minBorderRadius}"><img src="${item[this.label]}" >
				${this.indexShow ? `<span>${index + 1}/${this.swiperList.length}</span>` : ''}</div>`
		})

		this.dom.innerHTML = `<div class="swiper" style="width: ${this.width}px">
			<div class="img_box" id="img_box"  style="height: ${this.height}px"></div>
			
			<div class="bottom">
				<i class="icon left" style="height: ${this.minHeight}px; font-size: ${this.iconSize}" data-flag="true"><</i>
				<div class="scl_box"  style="width: ${this.width - this.width * 0.14 }px;height: ${this.minHeight}px">
					<div id="btn_my">
						${strHtml}
					</div>

				</div>
				<i class="icon right" style="height: ${this.minHeight}px;font-size: ${this.iconSize}"  data-flag="false">></i>
			</div>
		</div>`

		document.querySelectorAll(`${this.name} #btn_my>div`)[this.num].style.border = this.activeBorder
		document.querySelector(`${this.name} #img_box`).innerHTML =
			`<img  style="opacity: 1;${this.height}px;border-radius: ${this.borderRadius};" src="${this.swiperList[this.num][this.label]}" >`
		
		// 大图
		document.querySelector(`${this.name} #img_box img`).onclick = (e) => {
			this.maxClick(this.swiperList[this.num])
		}

		// 左箭头
		document.querySelector(`${this.name} .left`).onclick = (e) => {
			this.slideChange(e)
		}

		// 右箭头
		document.querySelector(`${this.name} .right`).onclick = (e) => {
			this.slideChange(e)
		}

		// 点击小图片
		this.swiperList.forEach((item, index) => {
			document.querySelectorAll(`${this.name} #btn_my img`)[index].onclick = (e) => {
				this.slideChange(e, index)
			}
		})

	}
	
	// 图片切换事件
	slideChange(e, index) {
		const flag = e.target.dataset.flag ? JSON.parse(e.target.dataset.flag) : '3'
		const left = this.minWidth + 12
		const width = document.querySelector(`${this.name} #btn_my`).getBoundingClientRect().width

		if (this.num >= 0) {
			if (Math.abs(flag ? this.num - 1 : this.num + 1) * left < width) {

				if (flag === true) {
					this.num--
				} else if (flag === false) {
					this.num++
				}

				if (this.num <= 0) {
					this.num = 0
				}

				if (flag == '3') {
					
					if (this.num === index) {
						return
					}
					this.num = index
				}

				
				if (this.width - this.width * 0.14 < width && (this.num * left + (this.width - this.width * 0.14)) <
					width) {
						this.newNumFlag = true
					document.querySelector(`${this.name} #btn_my`).style.left = -this.num * left + 'px'
				} else {
				
					this.newNum = Math.floor(((width - this.num * left + (this.width - this.width * 0.14)) / this
						.num) / left)


					if (Math.floor(((width - this.num * left + (this.width - this.width * 0.14)) / this.num) /
						left) > this.newNum) {

						document.querySelector(`${this.name} #btn_my`).style.left = -this.newNum * left + 'px'
					
					} {
						if (this.newNumFlag) {
							
							
							document.querySelector(`${this.name} #btn_my`).style.left = -this.recursionLeft(width, left) * left + 'px'
							this.newNumFlag = false
						}


					}
				}

				this.swiperList.forEach((item, index) => {
					document.querySelectorAll(`${this.name} #btn_my>div`)[index].style.border =
						'none'
				})

				document.querySelectorAll(`${this.name} #btn_my>div`)[this.num].style.border = this
					.activeBorder
					
				document.querySelector(`${this.name} #img_box`).innerHTML =
					`<img  src="${this.swiperList[this.num][this.label]}" style="height: ${this.height}px;border-radius: ${this.borderRadius};">`
				
				setTimeout(() => {
					document.querySelector(`${this.name} #img_box img`).style.opacity = 1
				}, 30)
				
				// 大图
				document.querySelector(`${this.name} #img_box img`).onclick = (e) => {
					this.maxClick(this.swiperList[this.num])
				}

			}
		} else {
			this.num = 0
		}

	}
	
	// 递归处理 left
	recursionLeft(width, left) {
		let num = 0
		const getNum = (width, left, i = 0) => {
			if (i * left + (this.width - this.width * 0.14) < width) {
				getNum(width, left, i + 1)
			} else {
				num = i
			}
		}
		getNum(width, left, num)
		
		return num
 	}
}
